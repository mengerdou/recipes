from django.urls import path

from recipes.views import (
    log_rating,
    RecipeDetaiView,
    RecipeListView,
    RecipeCreateView,
    RecipeUpdateView,
    RecipeDeleteView,
)

urlpatterns = [
    path("", RecipeListView.as_view(), name="recipes_list"),
    path("<int:pk>/", RecipeDetaiView.as_view(), name="recipe_detail"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    path("<int:pk>/update", RecipeUpdateView.as_view(), name="recipe_edit"),
    path("<int:pk>/delete", RecipeDeleteView.as_view(), name="recipe_delete"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
]
