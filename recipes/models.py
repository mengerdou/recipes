from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


# Create your models here.
class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(null=True, blank=True, auto_now=True)
    content = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Step(models.Model):
    order = models.SmallIntegerField()
    directions = models.TextField()
    author = models.ForeignKey(
        "Recipe", related_name="steps", on_delete=models.CASCADE
    )
    food_items = models.ManyToManyField("FoodItem", blank=True)

    def __str__(self):
        return "This is step" + str(self.order)


class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name + "----" + self.abbreviation


class FoodItem(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    amount = models.FloatField(
        validators=[MinValueValidator(0.0), MaxValueValidator(20.0)],
    )
    recipe = models.ForeignKey(
        "Recipe",
        related_name="ingredients",
        on_delete=models.CASCADE,
    )
    measure = models.ForeignKey(
        "Measure",
        on_delete=models.PROTECT,
    )
    food = models.ForeignKey(
        "FoodItem",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return "ingredient for " + str(self.recipe)


class Rating(models.Model):
    value = models.PositiveSmallIntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1),
        ]
    )
    recipe = models.ForeignKey(
        "Recipe",
        related_name="ratings",
        on_delete=models.CASCADE,
    )
